import yaml
import urllib.request
import os
import zipfile

def download(resources_folder):
    '''
    Download and extract datafiles described in the file INDEX.yaml in the given directory.
    '''
    rf = resources_folder
    for k,v in yaml.load(open(rf+'/INDEX.yml','r')).items():
        if not os.path.exists(rf+'/dl'):
            os.mkdir(rf+'/dl')
        if 'url' in v:
            dl_filename = rf+'/dl/' + v['download-filename']
            if not os.path.isfile(dl_filename):
                print('Downloading {} ...'.format(v['download-filename']),end='',flush=True)
                urllib.request.urlretrieve(v['url'],dl_filename)
                print('  OK')
            basename,extension = os.path.splitext(dl_filename)
            if not os.path.exists(basename):
                if extension=='.zip':
                    print('Extracting {} ...'.format(v['download-filename']),end='',flush=True)
                    with zipfile.ZipFile(dl_filename, 'r') as zip_ref:
                        zip_ref.extractall(path=basename)
                    print('  OK')
                elif extension=='.7z':
                    print('Extracting {} ...'.format(v['download-filename']),end='',flush=True)
                    os.system('7zr x '+dl_filename+' -o'+rf+'/dl -y >/dev/null')
                    print('  OK')

if __name__ == '__main__':
    download('resources')
