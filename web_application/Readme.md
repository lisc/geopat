
| Partenaires scientifiques | Partenaires acteurs | Subvention |
| :-----------------------: | :-----------------: | :--------: |
| [![logo Irstea](img/logoIrstea.png =100x*)](https://www.irstea.fr/fr)  [![logo UMR Territoires](img/logoTerritoires.jpg =90x*)](http://www.metafort.fr) | [![logo Grand Clermont ](img/logoGC.png =120x*)](http://www.legrandclermont.com/) [![logo PNR Livradois-Forez](img/logoPNRLF.png =80x*)](https://www.parc-livradois-forez.org/) | [![logo Clermont Auvergne Métropole](img/logoCAM.png)](https://www.clermontmetropole.eu) |

Contact : [Nicolas Dumoulin (Irstea)](mailto:nicolas.dumoulin@inrae.fr) − [Code Source](https://forgemia.inra.fr/lisc/geopat) (Licence libre)

[GéoPAT - final project report](https://hal.science/hal-04088344)

# Contexte

Cette application cartographique présente les résultats d'une étude qui s'inscrit dans le [Projet Alimentaire Territorial Grand-Clermont/PNR-Livradois-Forez](http://www.legrandclermont.com/projet-alimentaire-territorial) (PAT). Ce **PAT** a pour objectif de faire évoluer le système alimentaire « de la fourche à la fourchette » en améliorant le taux d'auto-approvisionnement du territoire et en offrant aux habitants les conditions d'une alimentation saine, de qualité, accessible à tous, issue de circuits de proximité et contribuant au développement d'une agriculture rémunératrice pour l'agriculteur et respectueuse de l'environnement.

Dans le cadre de ces travaux, une étude a permis de construire un scénario prospectif quantifiant l'évolution des surfaces agricoles pour chaque type de culture. Ce scénario prospectif intègre de nombreuses contraintes qui sont documentées dans la brochure du [programme d'actions du PAT](https://fr.calameo.com/read/0058048431bc37dfc14f8?page=3). L'étude a quantifié des évolutions de surfaces agricoles à l'horizon 2050 pour les différents types de culture comme sur la figure ci-dessous :

![Surfaces PAT 2050](img/ProjetPAT2050.png =600x260 "Scénario PAT")

# Le projet GéoPAT

À partir de ces surfaces agricoles calculées à l'échelle du territoire Grand-Clermont/PNR-Livradois-Forez, le projet GéoPAT a pour but de spatialiser ces réallocations de culture à une échelle plus fine pour observer les tendances locales. L'objectif est de répondre à la question : **Quels scénarios parcellaires correspondent au scénario PAT ?**

Concrètement, nous avons conçu un algorithme qui calcule des scénarios parcellaires qui permettent d'atteindre les objectifs quantifiés du scénario PAT présenté précédemment. À partir d'une répartition initiale des cultures sur l'ensemble des parcelles agricoles du territoire (Référentiel Parcellaire Géographique 2016, IGN/Licence ouverte), notre algorithme a généré des centaines de milliers de scénarios en réallouant aléatoirement des parcelles pour atteindre les surfaces globales du scénario PAT 2050. Parmi tous ces scénarios parcellaires, notre algorithme a sélectionné ceux qui permettent d'optimiser 5 critères :
 - **Productivité** : Utilisation de parcelles à valeur ajoutée élevée pour installer les cultures de fruits et légumes. Plus les nouvelles cultures de fruits et légumes sont allouées sur des parcelles ayant une altitude et une valeur foncière similaire aux valeurs actuelles, plus le scénario sera favorisé.
 - **Proximité** : Répartition géographique des fruits et légumes en rapport avec la population. Plus les cultures de fruits et légumes seront réparties sur les différents bassins de vie proportionnellement à la population, plus le scénario sera favorisé.
 - **Social** : Estimation du coût de changement de culture sur une parcelle. Pour chaque réallocation de parcelle, si l'exploitant(e) ne possède pas déjà la même culture sur une autre de ses parcelles et si la nouvelle culture est peu présente dans le bassin de vie (absence d'expertise locale, de filière de transformation) un coût est affecté selon la transition effectuée. Plus le coût total de transition d'un scénario est faible, plus le scénario sera favorisé.
 - **Résilience** : Diversification des cultures pour mieux résister aux agressions. Un indicateur statistique (indice de *Shannon*) permet d'évaluer le nombre de culture dans chaque maille de 5km par 5km. Plus un scénario aura des cultures diversifiées dans ces mailles, plus le scénario sera favorisé.
 - **Biodiversité** : Conservation des trames vertes lors de la réallocation de prairies. Un indicateur statistique (*Equivalent Connected Area*) permet de mesurer la connectivité des parcelles de prairies. Plus un scénario conservera des connexions entre prairies, plus le scénario sera favorisé.

Pour plus de détails, consultez le [rapport technique](https://hal.science/hal-04088344v1/document).

[//]: #( Limites :)
[//]: #(  - hydrographie et réseaux d'eau non pris en compte)
[//]: #(  - pas de prise en compte des rotations - parcellaire fixe, pas de nouvelles parcelles, pas de perte foncière)
