#!/usr/bin/python
# -*- coding: utf-8 -*-

import numpy as np
from scipy.sparse import csr_matrix, lil_matrix
from math import exp,log,pow
import os
from tqdm import tqdm
from Indicator import Indicator
from overrides import overrides
import patutils

class Biodiversity(Indicator):
	'''
	This class is used to compute the biodiversity indicator for a set of cultural
	patches.

	This indicator is computed with the equivalent connected area as described
	in (Saura et al., 2011):
	Santiago Saura, Christine Estreguil, Coralie Mouton, Mónica Rodríguez-Freire,
	Network analysis to assess landscape connectivity trends: Application to European
	forests (1990–2000), Ecological Indicators, Volume 11, Issue 2, 2011, Pages 407-416

	For each pair of patches a probability is computed with an decreasing exponential
	function P(i,j) = exp(-l*d). The coefficient l (lambda) is determined for a given
	minimal probability at the maximal distance d.
	'''
	@overrides
	def __init__(self, biodiversity_config, initial_patches, patches_md5sum, targetPAT=None):
		'''
		Creates an indicator with initial parameters
		The original file giving the patches is needed for checking the correspondance
		between this file and the matrix cached in a file.
		'''
		self.md5sum = patches_md5sum
		# the maximum distance threshold for considering a pair of patches
		self.dmax = biodiversity_config['dmax']
		#  the minimum probability threshold that should resulted of the exponential function at distance dmax
		self.epsilon = biodiversity_config['epsilon']
		self.final_result_ratio = pow(initial_patches['SURF_PARC'].sum(), 2) / 100000
		matrix_filename = biodiversity_config['matrixfilename']
		if not os.path.isfile(matrix_filename):
			self.compute_dispersal_probabilities(initial_patches)
			self.save_probabilities(matrix_filename)
		else:
			self.load_probabilities(matrix_filename)

	def compute_dispersal_probabilities(self, patches, progress=False):
		'''
		Computes the matrix of probabilities with given data.

		The resulting matrix is stored with a sparse matrix format in the probabilities attribute
		:param patches: a GeoDataFrame object containing the cultural patches
		'''
		# get the lambda value for given thresholds
		l = -log(self.epsilon, 2)/self.dmax
		# Creating a buffer for these patches, for finding patches under the distance threshold by intersection
		buffer = patches.buffer(self.dmax)
		index = buffer.sindex
		# sparse matrix initialisation
		self.probabilities = lil_matrix((len(patches),len(patches)), dtype='d')
		geom_idx=patches.columns.get_loc('geometry')
		print('Computing dispersal probabilities matrix (Biodiversity indicator) …')
		for i in tqdm(range(len(patches))):
			geom_i = patches.iat[i,geom_idx]
			for fid in list(index.intersection(geom_i.bounds)):
				# proba zero on the diagonal
				if i == fid:
					self.probabilities[fid,i] = 0
				if i >= fid:
					# the matrix is symetric, so we process only one half and
					# affect the proba on two sides
					d = patches.iat[fid,geom_idx].boundary.distance(geom_i.boundary)
					if d <= self.dmax:
						# patches i and fid have a distance less than the max distance
						p = patches.iat[fid,geom_idx].area*geom_i.area*l*exp(-l*d)
						self.probabilities[i,fid] = p
						self.probabilities[fid,i] = p
					else:
						self.probabilities[i,fid] = 0
						self.probabilities[fid,i] = 0
		self.probabilities = self.probabilities.tocsr()

	@overrides
	def compute_indicator(self, patches):
		'''
		Return the indice of biodiversity

		:param patches: List of PAT's patches and their data
		'''
		meadowmask = patches["cultgeopat"]==patutils.code_cultgeopat['Prairies']
		biodiversity = ((self.probabilities*meadowmask).T*meadowmask).sum()
		# We multiply by 100000 to get an indices with a value easy to understand for the users
		return self.final_result_ratio/biodiversity

	def save_probabilities(self, filename):
		'''
		Save the matrix of probabilities and initial parameters in a file (npz format).
		'''
		m = self.probabilities
		np.savez(filename, dmax=self.dmax, epsilon=self.epsilon, md5sum=self.md5sum,
			data=m.data, indices=m.indices, indptr=m.indptr, shape=m.shape)

	def load_probabilities(self, filename):
		'''
		Load the matrix of probabilities and initial parameters from a file (npz format).

		The correspondance will be checked between
		 class attributes and metadata stored in the file
		'''
		loader = np.load(filename)
		error_message = ('Matrix metadata {0} does\'nt correspond to metadata given at indicator construction'
			'\n\t{0} in metadata: {1} while asked: {2}'
			'\n\tmatrix filename: ') + filename
		for k1,k2 in [[self.dmax,'dmax'],[self.epsilon,'epsilon'],[self.md5sum,'md5sum']]:
			if k2 not in loader or k1!=loader[k2]:
				raise ValueError(error_message.format(k2, dict(loader.items()).get(k2, 'NA'), k1))
		self.probabilities = csr_matrix(
			(loader['data'], loader['indices'], loader['indptr']),
			shape=loader['shape']
		)

if __name__ == '__main__':
	from patutils import load_pat_patches, md5sum
	patches_filename = "../output/PAT_patches/PAT_patches.shp"
	patches = load_pat_patches(patches_filename)
	matrix_filename='../output/matrix_biodiversity.npz'
	biodiv = Biodiversity({'dmax': 1000, 'epsilon': 0.001, 'matrixfilename': matrix_filename}, patches, md5sum(patches_filename))
	print(biodiv.compute_indicator(patches))
