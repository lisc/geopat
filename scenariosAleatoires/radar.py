#!/usr/bin/python
# -*- coding: utf-8 -*-
import plotly
import plotly.plotly as py
import plotly.graph_objs as go


import plotly.plotly as py
import plotly.graph_objs as go

def scenarios_graph(number_of_scenar, dico):
	data=[]

	for i in range(number_of_scenar): 
		scenario = go.Scatterpolar(
		  r = [dico[i]["productivite"], dico[i]["resilience"], dico[i]["proximite"], dico[i]["biodiversite"], dico[i]["social"]],
		  theta = ["Productivité", "Résilience", "Proximité", "Biodiversité", "Social"],
		  fill = 'toself',
		  name = 'Scenario ' + str(i)
		)

		data.append(scenario)

		layout = go.Layout(
		polar = dict(
		radialaxis = dict(
		  visible = True,
		  range = [0, 100]
		)
		),
		showlegend = False
		)

		fig = go.Figure(data=data, layout=layout)
		plotly.offline.plot(fig, filename = "radar.html", auto_open=True)
'''
dico = {}
dico[0] = {}
dico[1] = {}

dico[0]["productivite"] = 3.5
dico[0]["resilience"] = 10
dico[0]["proximite"] = 3
dico[0]["biodiversite"] = 5
dico[0]["social"] = 1

dico[1]["productivite"] = 5
dico[1]["resilience"] = 1
dico[1]["proximite"] = 12
dico[1]["biodiversite"] = 10
dico[1]["social"] = 7

scenarios_graph(2, dico)
'''

