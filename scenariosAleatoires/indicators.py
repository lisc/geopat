#!/usr/bin/python
# -*- coding: utf-8 -*-

from overrides import overrides
from Indicator import Indicator
from proximite import Proximite as Proximity
from resilience_list import Resilience
from productivite import Productivity
from indice_biodiversite_2 import Biodiversity
from social import Social
import patutils

class CulturalIndicator(Indicator):
    @overrides
    def __init__(self, config, initial_patches=None, patches_md5sum=None, targetPAT=None):
        self.cultgeopat = patutils.code_cultgeopat[config]

    @overrides
    def compute_indicator(self, patches):
        return patches[patches['cultgeopat']==self.cultgeopat]['SURF_PARC'].sum()

class TargetDeltaIndicator(Indicator):
    '''
    This indicator computes the delta between the surfaces of cultural and the given target
    '''
    @overrides
    def __init__(self, config=None, initial_patches=None, patches_md5sum=None, targetPAT=None):
        self.targetPAT = targetPAT

    @overrides
    def compute_indicator(self, patches):
        surfDelta = self.targetPAT - patches.groupby('cultgeopat')['SURF_PARC'].sum()
        return surfDelta.abs().sum()

class Indicators:
    def __init__(self, config, initial_patches, patches_md5sum, targetPAT):
        self.indicators_names = ['Proximity', 'Resilience', 'Productivity', 'Biodiversity', 'Social']
        self._indicators = {
            indicator: eval(indicator)(config.get(indicator.lower()), initial_patches, patches_md5sum, targetPAT)
            for indicator in self.indicators_names
            }
        for cultgeopat in sorted(patutils.code_cultgeopat.keys()):
            self._indicators[cultgeopat] = CulturalIndicator(cultgeopat)
            self.indicators_names.append(cultgeopat)
        self._indicators['TargetDelta'] = TargetDeltaIndicator(targetPAT=targetPAT)
        self.indicators_names.append('TargetDelta')

    def compute_indicators(self, patches):
        return [self._indicators[ind].compute_indicator(patches) for ind in self.indicators_names]
