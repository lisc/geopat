#!/usr/bin/python
# -*- coding: utf-8 -*-

import abc

class Indicator:
    @abc.abstractmethod
    def __init__(self, config, initial_patches, patches_md5sum, targetPAT):
        pass

    @abc.abstractmethod
    def compute_indicator(self, patches):
        pass
