#!/usr/bin/python
# -*- coding: utf-8 -*-

import pandas as pd
import itertools
from patutils import load_pat_patches
from tqdm import tqdm

class Reallocator:
    def __init__(self, patches, targetPAT, ratioNbPatches):
        self.ratioNbPatches = ratioNbPatches
        self.targetPAT = targetPAT
        surfDelta = targetPAT - patches.groupby('cultgeopat')['SURF_PARC'].sum()
        self.PAT_cult_to_decrease = surfDelta[surfDelta<0].sort_values(ascending=True).keys().tolist()
        self.PAT_cult_to_increase = surfDelta[surfDelta>0].sort_values(ascending=False).keys().tolist()

    def reallocate(self, patches, rng):
        nbPatches = int(len(patches)*self.ratioNbPatches)
        surfDelta = self.targetPAT - patches.groupby('cultgeopat')['SURF_PARC'].sum()
        cult_to_decrease = surfDelta.loc[self.PAT_cult_to_decrease][surfDelta<0].sort_values(ascending=True).keys().tolist()
        cult_to_increase = surfDelta.loc[self.PAT_cult_to_increase][surfDelta>0].sort_values(ascending=True).keys().tolist()
        if len(cult_to_increase)==0 or len(cult_to_decrease)==0:
            # All cultural types have increased to objective, so not changing the patches
            return None
        # Sampling the patches to reallocate
        patches_to_decrease = patches[patches['cultgeopat'].isin(cult_to_decrease)]
        samples = patches_to_decrease.sample(n=min(nbPatches,len(patches_to_decrease)), random_state=rng)#.reset_index(drop=True)
        # Building the new culture reallocated
        factors = surfDelta[cult_to_increase]
        factors = (factors*len(samples)/factors.sum()).map(round) # normalize on nb samples
        newCult = pd.Series(cult_to_increase).repeat(factors)
        if len(newCult) < len(samples): # may be due to factors rounding
            newCult = newCult.append(newCult.sample(n=len(samples)-len(newCult), random_state=rng), ignore_index=True)
        newCult = newCult.sample(frac=1, random_state=rng)[:len(samples)].reset_index(drop=True) # shuffle and cut extra elements
        # Doing the reallocation
        patches.loc[samples.index.values,'cultgeopat'] = newCult.values
        surfDelta = self.targetPAT - patches.groupby('cultgeopat')['SURF_PARC'].sum()
        cult_to_decrease = surfDelta.loc[self.PAT_cult_to_decrease][surfDelta<0].sort_values(ascending=True).keys().tolist()
        cult_to_increase = surfDelta.loc[self.PAT_cult_to_increase][surfDelta>0].sort_values(ascending=True).keys().tolist()
        return [len(cult_to_increase)==0 or len(cult_to_decrease)==0, patches]

class ScenariosStack:

    def __init__(self, indicators, reallocator, initial_patches):
        self._counter = itertools.count()
        scenario_id = next(self._counter)
        self.indicators = indicators
        self.reallocator = reallocator
        self.initial_patches = initial_patches
        self.cultgeopat = initial_patches['cultgeopat'].to_frame().T
        # Now we have the id_parcel as columns, and each row will represent a scenario
        self.cultgeopat.rename(index={'cultgeopat':scenario_id}, inplace=True)
        self.initscores([self.indicators.compute_indicators(initial_patches) + [0, 0, -1, False, False]], [scenario_id], constructor=True)

    def initscores(self, scores, indexes, constructor=False):
        new_scores = pd.DataFrame(scores,
            columns=self.indicators.indicators_names+['iteration','cumulated_it', 'previous_index', 'pareto','full_reallocated'],
            index=indexes,
            dtype='float64')
        if constructor:
            self.scores = new_scores
        else:
            self.scores = pd.concat([self.scores[self.scores['full_reallocated']==True], new_scores], sort=False)
        for c in ['pareto','full_reallocated']:
            self.scores[c] = self.scores[c].astype('bool')
        for c in ['iteration','cumulated_it', 'previous_index']:
            self.scores[c] = self.scores[c].astype('int')

    def append(self, nb_iter, patches_cult, previous_index):
        id = next(self._counter)
        newdf = patches_cult['cultgeopat'].to_frame().T
        newdf.rename(index={'cultgeopat':id}, inplace=True)
        self.cultgeopat = pd.concat([self.cultgeopat,newdf])
        self.scores.loc[id] = self.indicators.compute_indicators(patches_cult) + [nb_iter, self.scores.loc[previous_index]['cumulated_it']+1, previous_index, False, False]
        for c in ['iteration','cumulated_it', 'previous_index']:
            self.scores[c] = self.scores[c].astype('int')
        return id

    def retain(self, mask):
        self.cultgeopat = self.cultgeopat[mask | (self.scores['full_reallocated'].to_list())]
        self.scores = self.scores[mask | (self.scores['full_reallocated'].to_list())]

    def setFullReallocated(self, id_scen, full_reallocated):
        self.scores.loc[id_scen,'full_reallocated'] = full_reallocated

    def isFullReallocated(self, id_scen):
        return self.scores.loc[id_scen,'full_reallocated']

    def nbFullReallocated(self):
        return len(self.scores[self.scores['full_reallocated']==True])

    def allFullReallocated(self):
        return self.scores[self.scores['full_reallocated']==False].empty

    def sample(self, nb, rng, mask=True):
        return self.cultgeopat[mask & (self.scores['full_reallocated']==False)].sample(nb, replace=True, random_state=rng)

    def consolidate_scenario(self, id, columns):
        selected_data = self.initial_patches[columns]
        return pd.concat([self.cultgeopat.loc[id].to_frame("cultgeopat"), selected_data],axis=1)

    def reconstitute(self, iter_nb, scenarios_patches, scenarios_cult, previous_indexes, fullreallocated, disable_progress=False):
        indexes = [next(self._counter) for id in range(len(scenarios_cult))]
        self.cultgeopat =  pd.concat([self.cultgeopat[self.scores['full_reallocated']==True],
            pd.DataFrame(scenarios_cult, index=indexes, columns=self.cultgeopat.columns, dtype='int8')
            ], sort=False)
        list_scores = []
        for i, previous_index in tqdm(enumerate(previous_indexes), total=len(previous_indexes), disable=disable_progress):
            scenario_patch = scenarios_patches[i]
            list_scores.append(self.indicators.compute_indicators(scenario_patch) + [iter_nb, self.scores.loc[previous_index]['cumulated_it']+1, previous_index, False, fullreallocated[i]])
        self.initscores(list_scores, indexes)

    def reallocate(self, scen_id, rng):
        if self.isFullReallocated(scen_id):
            return None
        else:
            patches = self.consolidate_scenario(scen_id, ['SURF_PARC', 'Bdv','init_cult', 'VALEUR_CAD', 'elevation'])
            return self.reallocator.reallocate(patches, rng)

    def reconstitute_mp(self, iter_nb, data, disable_progress=False):
        indexes = [next(self._counter) for id in range(len(data))]
        scenarios_cult = [d[2]['cultgeopat'] for d in data]
        self.cultgeopat =  pd.concat([self.cultgeopat[self.scores['full_reallocated']==True],
            pd.DataFrame(scenarios_cult, index=indexes, columns=self.cultgeopat.columns, dtype='int8')
            ], sort=False)
        list_scores = []
        for d in tqdm(data, total=len(data), disable=disable_progress):
            previous_index=d[0]
            scenario_patch = d[2]
            list_scores.append(d[3] + [iter_nb, self.scores.loc[previous_index]['cumulated_it']+1, previous_index, False, d[1]])
        self.initscores(list_scores, indexes)

    def reallocate_mp(self, scen_id, rng):
        if self.isFullReallocated(scen_id):
            return None
        else:
            patches = self.consolidate_scenario(scen_id, ['SURF_PARC', 'Bdv','init_cult', 'VALEUR_CAD', 'elevation'])
            [fullreallocated,scenario] = self.reallocator.reallocate(patches, rng)
            return [scen_id, fullreallocated,scenario, self.indicators.compute_indicators(scenario)]
