#!/usr/bin/python
# -*- coding: utf-8 -*-

import hashlib
import geopandas as gpd

def load_pat_patches(filename, encode_codecult=True):
    '''
    Load shapefile containing cultural patches and applying initial filters.
    '''
    patches = gpd.GeoDataFrame.from_file(filename, encoding='utf-8')
    for k in ['ID_PARCEL', 'INSEE_COM', 'CODE_EPCI', 'POPULATION','id_ilot','id_expl']:
        patches[k] = patches[k].astype('int32')
    patches.set_index('ID_PARCEL', inplace=True)
    patches = patches[patches['cultgeopat']!='Non Considérée']
    if encode_codecult:
        encode_cultgeopat(patches)
    return patches

indicators=['Proximity', 'Resilience', 'Productivity', 'Biodiversity', 'Social']

code_cultgeopat = {'Cultures industrielles': 0,
 'Céréales': 1,
 'Fourrages': 2,
 'Fruits et légumes': 3,
 'Oléagineux': 4,
 'Prairies': 5,
 'Protéagineux': 6}
reverse_code_cultgeopat = {v: k for k, v in code_cultgeopat.items()}

def encode_cultgeopat(patches):
    patches['cultgeopat'].replace(code_cultgeopat, inplace=True)
    patches['cultgeopat'] = patches['cultgeopat'].astype('int8')
    patches['init_cult'] = patches['cultgeopat']

def decode_cultgeopat(patches, column):
    patches[column].replace(reverse_code_cultgeopat, inplace=True)

def md5sum(filename):
    hash_md5 = hashlib.md5()
    with open(filename, "rb") as f:
        for chunk in iter(lambda: f.read(4096), b""):
            hash_md5.update(chunk)
    return hash_md5.hexdigest()
