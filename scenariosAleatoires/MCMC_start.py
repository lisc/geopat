#!/usr/bin/python
# -*- coding: utf-8 -*-
import pandas as pd
import geopandas as gpd
import sys

'''
This scripts intends to generate a scenario that fit at maximum one indicator at a time.
These scenarios will be used at starting points for the optimization process.
'''

def best_social(patches, surfDelta, cult_to_decrease, cult_to_increase):
    # Ensuring that Vegetables are at first position
    index_FL = cult_to_increase.index('Fruits et légumes')
    if index_FL>0:
        cult_to_increase[0],cult_to_increase[index_FL] = cult_to_increase[index_FL],cult_to_increase[0]
    # cult_to_increase
    expl_cult_list = {} # only needed for tests after the loop in debug mode
    # Doing reallocation for each culture needed surface, beginning by the greatest need
    for cult in cult_to_increase:
        # exploitant that have a patch of the needed culture
        expl_cult = patches[patches['cultgeopat'] == cult]['id_expl'].unique()
        expl_cult_list[cult] = expl_cult
        # Candidates for reallocation
        candidates = patches[ (patches['cultgeopat'].isin(cult_to_decrease)) & (patches['id_expl'].isin(expl_cult))]
        # surfcultbyexpl = candidates.groupby('id_expl')['SURF_PARC'].sum().sort_values(ascending=False)
        candidates = candidates.sort_values(by=['SURF_PARC'], ascending=False)
        # curexpl_idx = 0
        patch_idx = 0
        # looping on patches of culture in excess
        while patch_idx<len(candidates) and surfDelta[cult]>0:
            index = candidates.index[patch_idx]
            patchCult = candidates.at[index,'cultgeopat']
            patch_idx += 1
            # if the culture is always in excess
            if surfDelta[patchCult] < 0:
                area = candidates.at[index,'SURF_PARC']
                if area > surfDelta[cult]: # patch is bigger than needed
                    if (area - surfDelta[cult])/surfDelta[cult] > 0.001: # patch is significantly bigger (10%)
                        if patch_idx<len(candidates)-1: # this is not the last choice
                            nextarea = candidates.at[candidates.index[patch_idx],'SURF_PARC']
                            if abs(surfDelta[cult]-area) > abs(surfDelta[cult]-nextarea):
                            # the next one don't give a smaller difference
                                continue
                patches.at[index,'cultgeopat'] = cult
                surfDelta[cult] -= area
                surfDelta[patchCult] += area
    # import pdb; pdb.set_trace()
    return patches

def best_proximity(patches, surfDelta):
    Population_PAT = patches.groupby('INSEE_COM')['POPULATION'].first().sum()
    popByBdv = patches.groupby('Bdv').apply(lambda x:x.groupby('INSEE_COM')['POPULATION'].first().sum())
    targetSurfByBdv = surfDelta['Fruits et légumes'] * popByBdv/Population_PAT
    for bdv, grouped in patches.groupby('Bdv'):
        pass
        # TODO


if __name__ == '__main__':
    patches = gpd.GeoDataFrame.from_file('../output/PAT_patches/PAT_patches.shp', encoding='utf-8')
    patches = patches[patches['cultgeopat']!='Non Considérée']
    target = pd.read_csv('../resources/targetPAT.csv', sep=';',index_col=0)
    targetRatio = (target['2050']-target['2016'])/target['2016']
    targetPAT = patches.groupby('cultgeopat')['SURF_PARC'].sum()*(1+targetRatio)
    surfDelta = patches.groupby('cultgeopat')['SURF_PARC'].sum()*targetRatio
    cult_to_decrease = targetRatio[targetRatio<0].sort_values(ascending=True).keys().tolist()
    cult_to_increase = targetRatio[targetRatio>0].sort_values(ascending=False).keys().tolist()
    surfDelta_comparison = surfDelta.to_frame('before')
    import time
    start = time.time()
    patches2 = best_social(patches, surfDelta, cult_to_decrease, cult_to_increase)
    end = time.time()
    print('Time = {}'.format(end-start))
    surfDelta_comparison['social'] = surfDelta
    print(surfDelta_comparison)
    # patches2.to_file('../output/PAT_patches/best_social.shp', encoding='utf-8')
