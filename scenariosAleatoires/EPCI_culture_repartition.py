#!/usr/bin/python
# -*- coding: utf-8 -*-

import numpy as np
from scipy.sparse import csr_matrix, lil_matrix
from math import exp,log
from tqdm import tqdm
import geopandas as gpd



def EPCI_culture_repartition(EPCI, scenario):
	'''
	This function calculate the area for each type of culture for each EPCI and return it as a dictionary.
	It will be mostly used in the web application.
	
	:param EPCI: ShapeFile of EPCI converts to a list
	:param scenario: 
	'''
	
	EPCI_layer = gpd.GeoDataFrame.from_features(EPCI)
	scenario_layer = gpd.GeoDataFrame.from_features(scenario)
	
	index = scenario_layer.sindex
	 
	dict_EPCI = {}
	for i in range(len(EPCI_layer)) :
		dict_EPCI[i] = {}
		dict_EPCI[i]["properties"] = {}
		#dict_EPCI[i]["geometry"] = EPCI_layer.iloc.geometry
		dict_EPCI[i]["properties"]['CODE_EPCI'] = EPCI_layer.iloc[i].CODE_EPCI
		dict_EPCI[i]["properties"]['NOM_EPCI'] = EPCI_layer.iloc[i].NOM_EPCI
		dict_EPCI[i]["properties"]['POPULATION'] = EPCI_layer.iloc[i].POPULATION
		dict_EPCI[i]["properties"]['Cereales'] = 0
		dict_EPCI[i]["properties"]['Cultures industrielles'] = 0
		dict_EPCI[i]["properties"]['Fourrages'] = 0
		dict_EPCI[i]["properties"]['Fruits et legumes'] = 0
		dict_EPCI[i]["properties"]['Oleagineux'] = 0
		dict_EPCI[i]["properties"]['Prairies'] = 0
		dict_EPCI[i]["properties"]['Proteagineux'] = 0
		dict_EPCI[i]["properties"]['None'] = 0

	for i in range(len(EPCI_layer)):
		geom_i = EPCI_layer.iloc[i].geometry
		for fid in list(index.intersection(geom_i.bounds)) :
			if scenario_layer.iloc[fid].rpg_lfgc_s == None :
				dict_EPCI[i]["properties"]["None"] += scenario_layer.iloc[fid].SURF_PARC
			else :
				dict_EPCI[i]["properties"][scenaro_layer.iloc[fid].rpg_lfgc_s] += scenario_layer.iloc[fid].SURF_PARC
		
	return dict_EPCI
