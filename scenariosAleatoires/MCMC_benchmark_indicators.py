from MCMC import *
from scenarios import ScenariosStack
import timeit

if __name__ == '__main__':
    mcmc = MCMC('MCMC_config.yml', False)
    scenarios = ScenariosStack(mcmc.indicators, mcmc.reallocator, mcmc.patches)
    for ind in ['Proximity', 'Resilience', 'Productivity', 'Biodiversity', 'Social']:
        print('{}: {:.4f} s'.format(ind,
            timeit.timeit(lambda: eval('mcmc.indicators._indicators[\''+ind+'\'].compute_indicator(mcmc.patches)'), number=10)/10
            ))
    print('\nreallocate: {:.4f} s'.format(timeit.timeit(lambda:
        mcmc.reallocator.reallocate(mcmc.patches, mcmc.rng)
        , number=10)/10))
    for ind in ['Proximity', 'Resilience', 'Productivity', 'Biodiversity', 'Social']:
        print('{}: {:.4f} s'.format(ind,
            timeit.timeit(lambda: eval('mcmc.indicators._indicators[\''+ind+'\'].compute_indicator(mcmc.patches)'), number=10)/10
            ))


'''
Proximity: 0.0137 s
Resilience: 0.0311 s
Productivity: 0.0022 s
Biodiversity: 0.0120 s
Social: 0.0841 s

reallocate: 0.0280 s
Proximity: 0.0167 s
Resilience: 0.0322 s
Productivity: 0.0049 s
Biodiversity: 0.0120 s
Social: 0.1015 s
'''
