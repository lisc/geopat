#!/usr/bin/python
# -*- coding: utf-8 -*-
from Indicator import Indicator
from overrides import overrides
import patutils

class Proximite(Indicator):
	'''
	Function calculating the proximity indice.
	The purpose of this indice is to reflect the proximity between productions site and consommation site.
	We are calculating it with a comparison of the area of patches cultivating fruits and vegetables
	 and the population of living area (bassin de vie) in the PAT's territory

	A living area with a low population will require a lower cultivating area to get a good value.
	'''
	@overrides
	def __init__(self, config, initial_patches, patches_md5sum=None, targetPAT=None):
		Population_PAT = initial_patches.groupby('INSEE_COM')['POPULATION'].first().sum() #498873
		popByBdv = initial_patches.groupby('Bdv').apply(lambda x:x.groupby('INSEE_COM')['POPULATION'].first().sum())
		# OK but slower
		# popByBdv = patches.groupby(['Bdv','INSEE_COM']).first().groupby('Bdv')['POPULATION'].sum()
		self.targetSurfByBdv = targetPAT[patutils.code_cultgeopat['Fruits et légumes']] * popByBdv/Population_PAT

	@overrides
	def compute_indicator(self, patches):
		'''
		:param shape: The scenario to analyse as a list
		:param fruit_legume: The proportion of area we want in the PAT for the "fruits and vegetables" type
		:param affichage: True if we want the display in the console, False is the other case
		'''
		# get area of "fruits et légumes" in the scenario
		flSurfByBdv = patches[patches['cultgeopat']==patutils.code_cultgeopat['Fruits et légumes']].groupby('Bdv').agg({'SURF_PARC':sum})
		result = flSurfByBdv.div(self.targetSurfByBdv,axis=0).fillna(0)
		if result.where(result<1).count().sum() == 0:
			# All Bdv are fullfilled
			result = 1
		else:
			# put 1 where target is reached and computing mean
			result = result.where(result<1, 1).sum() / result.count()
			result = result.values[0]
		return 1/result

if __name__ == '__main__':
	import pandas as pd
	from patutils import load_pat_patches
	patches_filename = "../output/PAT_patches/PAT_patches.shp"
	patches = load_pat_patches(patches_filename)
	target = pd.read_csv('../resources/targetPAT.csv', sep=';',index_col=0).rename(index=patutils.code_cultgeopat)
	targetRatio = (target['2050']-target['2016'])/target['2016']
	targetPAT = patches.groupby('cultgeopat')['SURF_PARC'].sum()*(1+targetRatio)
	prox = Proximite(None, patches, None, targetPAT)
	print(prox.compute_indicator(patches))
