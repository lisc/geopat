#!/usr/bin/python3
# -*- coding: utf-8 -*-
import geopandas as gpd
import pandas as pd
import numpy as np
import math
from Indicator import Indicator
from overrides import overrides

class Resilience(Indicator):
	@overrides
	def __init__(self, resilience_config, initial_patches, patches_md5sum=None, targetPAT=None):
		'''
		Initialize the indicator.

		:param grille: Grid divising the PAT in squares
		'''
		self.grid = gpd.GeoDataFrame.from_file(resilience_config)
		# caching intersection between patches and grid
		self.intersection = gpd.sjoin(
			self.grid[['id','geometry']],
		 	initial_patches[['geometry','SURF_PARC']].copy(),
			how='inner', op='intersects')
		self.intersection.rename(columns={'index_right':'ID_PARCEL'}, inplace=True)
		# caching surface of patches in each cell
		self.intersection['surf_cell'] = self.intersection[['id','SURF_PARC']].groupby('id').transform(sum)
		self.size = len(self.intersection.groupby('id'))

	# The parameters are : the grid layer, a list corresponding to a scenario and a boolean value True or False signifying if we want to see the printing lines or not
	@overrides
	def compute_indicator(self, patches):
		'''
		This function calculate the resiliency indice

		We are using the Shannon's indices (1949) to evaluate  the heterogeneity and the diversity of the cultural patches of PAT.
		This indice consist of cutting the territory we are working on in square and determine the diversity of the patches in it.

		We decided to cut the PAT territory in 5km by 5km square

		Shannon's indice : ∑ pi*log2(pi)
		pi: a specific kind of culture      ∑: for each kind of culture

		:param patches: The scenario to analyse as a list
		'''
		# merging given patches with table of intersection patches/grid
		intersection2 = pd.merge(self.intersection, patches[['cultgeopat','SURF_PARC']],left_on='ID_PARCEL', right_index=True,how='left')
		# building pivot table on grid cell and cultgeopat
		pivot = pd.pivot_table(intersection2,values=['SURF_PARC_x','surf_cell'],index=['id','cultgeopat'],aggfunc={'SURF_PARC_x':np.sum,'surf_cell':'first'})
		pivot['res'] = pivot['SURF_PARC_x']/pivot['surf_cell']
		return - self.size / (pivot['res'] * pivot['res'].apply(math.log2)).sum()

if __name__ == '__main__':
	from patutils import load_pat_patches, code_cultgeopat
	patches_filename = "../output/PAT_patches/PAT_patches.shp"
	patches = load_pat_patches(patches_filename)
	import time
	res = Resilience("Parcelle_PAT/Grille_resilience.shp", patches)
	start = time.time()
	print(res.compute_indicator(patches))
	elapsed = time.time()-start
	print(elapsed)
	cult_col_index = patches.columns.get_loc('cultgeopat')
	for i in range(5000):
		patches.iat[i,cult_col_index]=code_cultgeopat['Céréales']
	print(res.compute_indicator(patches))
