#!/usr/bin/python3
# -*- coding: utf-8 -*-
import geopandas as gpd
import pandas as pd
from Indicator import Indicator
from overrides import overrides
import patutils
from scipy.stats import gaussian_kde

class Productivity(Indicator):
	'''
	This indicator calculate the productivity indice.

	The cadastral's values of the patches of the PAT's territory are used to get an approximation idea of the value of each patch.
	The indice reflect the value of the patch used to cultivate the fruits and vegetables.
	'''
	@overrides
	def __init__(self, social_config=None, initial_patches=None, patches_md5sum=None, targetPAT=None):
		self.elevationKernel = gaussian_kde(initial_patches[initial_patches['cultgeopat']==patutils.code_cultgeopat['Fruits et légumes']]['elevation'])

	@overrides
	def compute_indicator(self, layer_scenario):
		fl = layer_scenario[layer_scenario['cultgeopat']==patutils.code_cultgeopat['Fruits et légumes']]
		valeurs_cad = self.elevationKernel(fl['elevation'])*fl['VALEUR_CAD'] / fl['SURF_PARC']
		return len(valeurs_cad) / valeurs_cad.sum()

if __name__ == '__main__':
	import geopandas as gpd
	from patutils import load_pat_patches
	patches = load_pat_patches("../output/PAT_patches/PAT_patches.shp")
	prod = Productivity()
	print(prod.compute_indicator(patches))
