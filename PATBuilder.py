#!/usr/bin/python
# -*- coding: utf-8 -*-

import numpy as np
import pandas as pd
import geopandas as gpd
import os, glob
import rasterio
import json

def load_MAJIC(majic_dir):
    patches = gpd.GeoDataFrame()
    for f in glob.glob(majic_dir+'/*.shp'):
        patches.append(gpd.GeoDataFrame.from_file(f))
    return patches

def build_initial_PAT(rpg_parcelles_filename, rpg_ilots_filename, pat_cultural_classes_filename, majic_dir,
    bdalti_dir, bdalti_tiles, pat_municipalities_dir, bdv_filename, bdv_fixes_filename,
    adminexpress_com_filename, adminexpress_epci_filename):
    '''
    This method build the initial patches set by consolidating the data sources
    used by the project:
     - RPG: Registre Parcellaire Graphique
     - PAT municipalities: list of csv files containing for each administrative sets (EPCI, PNR), the list of municipalities considered
     - PAT cultural classes: give the classification of cultural code of RPG considered for the PAT
     - AdminExpress: datasets containing the shapes and population of municipalities
     - BDV: list of "Bassins de vie" that aggregates municipalities in functional sets
     - MAJIC: landed property data, used for extracting "revenu cadastral" for each patch

     Returns an object
    '''
    municipalities = build_PAT_municipalities(pat_municipalities_dir, bdv_filename, bdv_fixes_filename,
        adminexpress_com_filename, adminexpress_epci_filename)
    municipalities = municipalities[['INSEE_COM','Nom','CODE_EPCI','NOM_EPCI', 'geometry','POPULATION','BV2012','LIBBV2012']]
    rpg = gpd.GeoDataFrame.from_file(rpg_parcelles_filename, encoding='utf-8')[['ID_PARCEL','CODE_CULTU','geometry']]
    # we will use the centroid for doing the sjoin with municipalities
    rpg['patches'] = rpg.geometry
    rpg.geometry = rpg.geometry.centroid
    patches = gpd.sjoin(rpg, municipalities, op='intersects') # 'op' is useless because sjoin between points and polygons
    # sjoin with rpg_ilots for retrieving id of "exploitant"
    rpg_ilots = gpd.GeoDataFrame.from_file(rpg_ilots_filename, encoding='utf-8')[['id_ilot','id_expl','surf_ilot','geometry']]
    # restoring geometry instead of centroid
    patches.geometry = patches['patches']
    del patches['patches']
    del patches['index_right']
    patches_expl = gpd.sjoin(patches, rpg_ilots, how='left', op='intersects')
    # removing orphan patches (without exploitant)
    orphan_patches = patches_expl[patches_expl['id_ilot'].isnull()]
    print('{} patches without "id_expl" ({:.4f} % of total area)'.format(
        len(orphan_patches),
        orphan_patches.geometry.area.sum() / patches_expl.geometry.area.sum()
        ))
    patches_expl = patches_expl[~patches_expl['id_ilot'].isnull()]
    patches_expl.geometry = patches_expl.geometry.buffer(0)
    rpg_ilots.geometry = rpg_ilots.geometry.buffer(0)
    # selecting the greatest intersection between duplicates patches from sjoin
    patches_expl['intersection_surf'] = patches_expl.apply(
       lambda row: rpg_ilots[rpg_ilots['id_ilot']==row['id_ilot']]['geometry'].intersection(row['geometry']).area.sum(),
       axis=1)
    patches_expl['max_intersection_surf'] = patches_expl.groupby('ID_PARCEL')['intersection_surf'].transform(max)
    patches_expl = patches_expl[patches_expl['max_intersection_surf'] == patches_expl['intersection_surf']]
    # cleaning
    del patches_expl['max_intersection_surf']
    del patches_expl['intersection_surf']
    del patches_expl['index_right']
    patches_expl['id_ilot'] = patches_expl['id_ilot'].astype(int)
    patches_expl['id_expl'] = patches_expl['id_expl'].astype(int)
    # for the last remaining duplicates, we select the "ilot" from the "exploitant" that have the greatest surface
    patches_expl['expl_surf'] = patches_expl.apply(
        lambda row: patches_expl[patches_expl['id_expl']==row['id_expl']]['surf_ilot'].sum(),
        axis=1)
    patches_expl['max_expl_surf'] = patches_expl.groupby('ID_PARCEL')['expl_surf'].transform(max)
    patches_expl = patches_expl[patches_expl['max_expl_surf'] == patches_expl['expl_surf']]
    del patches_expl['max_expl_surf']
    del patches_expl['expl_surf']
    # cultural code joining
    codes = pd.read_csv(pat_cultural_classes_filename)
    patches_expl = patches_expl.merge(codes[['category','Classe GéoPAT']],how='left',left_on='CODE_CULTU',right_on='category')
    patches_expl = patches_expl.rename(columns={
        'Classe GéoPAT':'cultgeopat',
        'BV2012':'Bdv'
        })
    # Adding elevation from BD Alti
    rasters = [] # reading tiles
    for tile in bdalti_tiles:
        dataset = rasterio.open(bdalti_dir+tile+'.asc')
        rasters.append([dataset, dataset.read(1)])
    def getElevation(point): # Function for getting the elevation from a point
        # Looping on every loaded rasters
        for raster in rasters:
            coords = raster[0].index(point.x, point.y)
            # If coords are in bounding box, returning the elevation
            if coords[0] >= 0 and coords[1] >= 0 and coords[0] < 1000 and coords[1] < 1000:
                return raster[1][coords]
    patches_expl['elevation'] = patches_expl.geometry.centroid.apply(getElevation)
    # TODO join with MAJIC
    patches_expl['SURF_PARC'] = patches_expl.area
    return [patches_expl, municipalities]

def build_PAT_municipalities(pat_municipalities_dir, bdv_filename, bdv_fixes_filename,
        adminexpress_com_filename, adminexpress_epci_filename):
    # Read list of considered municipalities
    municipalities = pd.DataFrame()
    for f in glob.glob(pat_municipalities_dir+'/*.csv'):
        df=pd.read_csv(f)
        df[os.path.splitext(os.path.basename(f))[0]] = 'True'
        if municipalities.empty:
            municipalities = df
        else:
            municipalities = pd.merge(municipalities,df,on=['Code Insee','Nom'],how='outer')
    municipalities = municipalities.fillna('False')
    municipalities['Code Insee']=municipalities['Code Insee'].astype(str)
    # Load "Bassins de vie"
    bvxls=pd.read_excel(bdv_filename,sheetname='Composition_communale', header=5)
    municipalities = pd.merge(municipalities,bvxls[['CODGEO','BV2012','LIBBV2012']],left_on='Code Insee',right_on='CODGEO')
    del municipalities['CODGEO']
    # fixes for "Bassins de vie"
    bdvfixes=pd.read_csv(bdv_fixes_filename, delimiter=';').astype(str)[['Code Insee','BV2012','LIBBV2012']]
    municipalities = pd.merge(municipalities, bdvfixes, how='left', on='Code Insee', suffixes=('_old',''))
    municipalities['BV2012'] = municipalities['BV2012'].fillna(municipalities['BV2012_old'])
    municipalities['LIBBV2012'] = municipalities['LIBBV2012'].fillna(municipalities['LIBBV2012_old'])
    # Load shapes and population
    admin_express_com = gpd.GeoDataFrame.from_file(
            adminexpress_com_filename, encoding='utf-8'
        )[['INSEE_COM','geometry','POPULATION','CODE_EPCI']]
    municipalities = pd.merge(admin_express_com,municipalities,how='inner',left_on='INSEE_COM',right_on='Code Insee')
    del municipalities['Code Insee']
    # load EPCI
    admin_express_epci = gpd.GeoDataFrame.from_file(adminexpress_epci_filename)[['CODE_EPCI','NOM_EPCI']]
    municipalities = municipalities.merge(admin_express_epci, how='inner', on='CODE_EPCI')
    return municipalities

def buildHexagons(patches, muns, hexagonsFilename):
    muns.crs={'init':'epsg:2154'}
    hexagons = gpd.GeoDataFrame.from_file(hexagonsFilename)
    hexagonsPAT=gpd.sjoin(hexagons, muns[['geometry']], op='intersects')
    # drop duplicates on index
    hexagonsPAT.reset_index(inplace=True)
    hexagonsPAT.drop_duplicates(subset='index', inplace=True)
    # keep only geometry
    hexagonsPAT.drop(['index','left','bottom','right','top','index_right'], axis=1, inplace=True)
    hexagonsPAT.reset_index(drop=True, inplace=True)
    # Get indexes as a columns for keeping indexes after 'overlay' call
    hexagonsPAT.rename_axis('FID', inplace=True)
    hexagonsPAT.reset_index(inplace=True)
    # Intersection between hexagons and patches
    patches_inter_hexagons = gpd.overlay(patches[['ID_PARCEL','geometry','id_expl','elevation', 'cultgeopat','INSEE_COM']], hexagonsPAT.reset_index(), how="intersection")
    patches_inter_hexagons.to_file('output/hexagons/patches_inter_hexagons.shp')
    # computes area of intersection
    patches_inter_hexagons['area_ha'] = patches_inter_hexagons.area / 10000
    # Adding elevation mean
    patches_inter_hexagons['tmp'] = patches_inter_hexagons['elevation']*patches_inter_hexagons['area_ha']
    grouped = patches_inter_hexagons.groupby('FID')[['tmp','area_ha']].sum()
    hexagonsPAT['elevation_mean'] = grouped['tmp']/grouped['area_ha']
    # remove hexagons without intersection with patches
    hexagonsPAT = hexagonsPAT[~hexagonsPAT['elevation_mean'].isnull()]
    # Adding number of exploitants
    hexagonsPAT['nb_expl'] = patches_inter_hexagons.groupby('FID')['id_expl'].nunique()
    # Adding initial cultural surfaces
    surfaces = patches_inter_hexagons.groupby('FID').apply(lambda x:x.groupby('cultgeopat')['area_ha'].sum())
    surfaces = surfaces.reset_index().pivot(index='FID', columns='cultgeopat', values='area_ha')
    surfaces.columns = ['init_surf_cult' + str(col) for col in surfaces.columns]
    surfaces.fillna(0, inplace=True)
    hexagonsPAT = hexagonsPAT.join(surfaces)
    hexagonsPAT.to_file('output/hexagons/hexagons1000PAT.shp')
    # Extracting list of municipalities included in each hexagon and the cumulated area of patches
    areasByHexByMunDataFrame = patches_inter_hexagons.groupby('FID').apply(lambda h: h.groupby('INSEE_COM').apply(lambda p:p.area.sum() / 10000)).to_frame('area_ha')
    # Converting as a dict
    areasDict = areasByHexByMunDataFrame.groupby(level=0).apply(lambda dff:dff.xs(dff.name)['area_ha'].to_dict()).to_dict()
    areasDict['__README']='This dict gives area of patches (in ha) for each municipality in each hexagon. The first key is the ID of the hexagon.'
    # Writing in file
    with open("output/hexagons/hexagons_muns.json", "w") as output:
        json.dump(areasDict, output)
    return hexagonsPAT

if __name__ == '__main__':
    import yaml
    try:
        from yaml import CLoader as Loader
    except ImportError:
        from yaml import Loader
    resources={}
    config = yaml.load(open('resources/INDEX.yml','r'), Loader=Loader)
    for k,v in config.items():
        resources[v['variable']] = 'resources/'+v['file'] if 'file' in v else k
    [patches, municipalities] = build_initial_PAT(
        resources['rpg_parcelles_filename'],
        resources['rpg_ilots_filename'],
        resources['pat_cultural_classes_filename'],
        resources['majic_filename'],
        resources['bdalti_dir'], config['BDALTI']['tiles'],
        resources['pat_municipalities_dir'],
        resources['bdv_filename'], resources['bdv_fixes'], resources['adminexpress_com_filename'],
        resources['adminexpress_epci_filename'])
    # TODO merge with MAJIC data not automatic
    # FIXME use raw data instead these preprocessed data
    majic = gpd.GeoDataFrame.from_file('Productivite/productivite_shape/Parcelle_PAT_valCad_test.shp', encoding='utf-8')[['ID_PARCEL','VALEUR_CAD']]
    patches = patches.merge(majic,how='left',on='ID_PARCEL')
    # some missing data due to the preprocessed data
    patches = patches[~patches['VALEUR_CAD'].isnull()]

    patches.to_file('output/PAT_patches', encoding='utf-8')
    gpd.GeoDataFrame(municipalities).to_file('output/municipalities', encoding='utf-8')
    buildHexagons(patches, municipalities, resources['hexagons_filename'])
