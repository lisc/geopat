#!/usr/bin/python3
# -*- coding: utf-8 -*-
from shapely.geometry import mapping, shape
from shapely.ops import cascaded_union
import fiona
from fiona import collection
import math

# Transforme le nom du type de culture en un code utilisé dans l'algorithme
def return_culture_code(culture):
	if culture == "CÃ©rÃ©ales".decode('utf-8') :
		return 0

	if culture == "Cultures industrielles" :
		return 1

	if culture == "Fourrages" :
		return 2

	if culture == "Fruits et lÃ©gumes".decode('utf-8') :
		return 3

	if culture == "OlÃ©agineux".decode('utf-8') :
		return 4

	if culture == "Prairies" :
		return 5

	if culture == "ProtÃ©agineux".decode('utf-8') :
		return 6
# Donne la moyenne de la valeur cadastrale au mètre carré du scénario donné pour chacun des types de parcelle
def indice_productivite(shape_scenario, shape_valeur_cadastrale, statsOutputFilename) :
	liste_i = {}
	culture_tab = ["Céréales", "Cultures industrielles", "Fourrages", "Fruits et légumes", "Oléagineux", "Prairies", "Protéagineux" ]

	for i in range(7): # Initialise la liste des parcelles pour chacun des types
		liste_i[i] = []

	with fiona.open(shape_scenario, 'r') as layer_scenario : # Ouvre la couche scenario
		for i in range(len(layer_scenario)): # Pour chacune des parcelles du scenario

			if layer_scenario[i]["properties"]["rpg_lfgc_s"] != None : # Si le type de culture est non nul
				liste_i[return_culture_code(layer_scenario[i]["properties"]["rpg_lfgc_s"])].append(i) # Ajouter son FID dans la liste en fonction de son type

	with fiona.open(shape_valeur_cadastrale, 'r') as layer_valeur_cadastrale: # Ouvre la couche des valeurs cadastrales
		with open(statsOutputFilename,'w') as statsFile:
			statsFile.write('type culture;valeur cadastrale moyenne par m²')
			for y in range(7) : # Pour chacun des types de culture
				somme_valeur_cadastrale = 0.0 # Initialisation de sa valeur cadastrale
				for i in liste_i[y]: # Pour chacune des parcelles du type numéro 'y' (voir fonction return_culture_code)
					valeur_cadastrale =  layer_valeur_cadastrale[i]["properties"]["VALEUR_CAD"]
					surf_parcelle = shape(layer_valeur_cadastrale[i]["geometry"]).area

					somme_valeur_cadastrale += valeur_cadastrale / surf_parcelle # Incrémentation de la valeur cadastrale en m2
				if len(liste_i[y]) != 0 : # Si il y a au moins une parcelle possédant le type numéro 'y'
					statsFile.write("{};{}".format(culture_tab[y],somme_valeur_cadastrale/len(liste_i[y])))
					print("La moyenne des valeurs cadastrales pour les parcelles de type " + str(culture_tab[y]) + " est de " + str(somme_valeur_cadastrale/len(liste_i[y])) + " par mètre carré")

indice_productivite("./Parcelle_PAT/Parcelle_PAT.shp", "./productivite_shape/Parcelle_PAT_valCad_test.shp", "productivite.csv")
