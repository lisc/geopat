#!/usr/bin/python
# -*- coding: utf-8 -*-
from shapely.geometry import mapping, shape, Polygon
from shapely.ops import cascaded_union
from shapely.wkt import loads
import fiona
from fiona import collection
import random
from random import randint
import rtree
from collections import deque

with fiona.open('./42_jointure_valCad.shp', 'r') as input42:
	with fiona.open('./43_jointure_valCad.shp', 'r') as input43:
		with fiona.open('./63_jointure_valCad.shp', 'r') as input63:
			schema = {'geometry': 'Polygon', 'properties': {'idpar' : 'str', 'idcom' : 'str', 'valeur_cadastrale' : 'int'}}
			crs = input42.crs
			driver = input42.driver
			
			with fiona.open('./valeurs_cadastrales.shp','w',driver=driver, crs=crs, schema=schema) as file_valeurs_cadastrales :

				for f in input42 :
					recW = {}
					recW["geometry"]=f["geometry"]
					recW["properties"] = {}
					
					recW["properties"]["idpar"] = f["properties"]["idpar"]
					recW["properties"]["idcom"] = f["properties"]["idcom"]
					recW["properties"]["valeur_cadastrale"] = f["properties"]["inst42_201"]
					
					file_valeurs_cadastrales.write(recW)
					
				for f in input43 :
					recW = {}
					recW["geometry"]=f["geometry"]
					recW["properties"] = {}
					
					recW["properties"]["idpar"] = f["properties"]["idpar"]
					recW["properties"]["idcom"] = f["properties"]["idcom"]
					recW["properties"]["valeur_cadastrale"] = f["properties"]["inst43_201"]
					
					file_valeurs_cadastrales.write(recW)
					
				for f in input63 :
					recW = {}
					recW["geometry"]=f["geometry"]
					recW["properties"] = {}
					
					recW["properties"]["idpar"] = f["properties"]["idpar"]
					recW["properties"]["idcom"] = f["properties"]["idcom"]
					recW["properties"]["valeur_cadastrale"] = f["properties"]["inst63_201"]
					
					file_valeurs_cadastrales.write(recW)
			
		

