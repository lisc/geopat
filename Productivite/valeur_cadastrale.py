#!/usr/bin/python
# -*- coding: utf-8 -*-
from shapely.geometry import mapping, shape, Polygon
from shapely.ops import cascaded_union
from shapely.wkt import loads
from shapely.validation import explain_validity
import fiona
from fiona import collection
import random
from random import randint
import rtree
import re



with fiona.open('./Parcelle_PAT/Parcelle_PAT.shp', 'r') as inputShape:

	schema = {'geometry': 'Polygon', 'properties': {'ID_PARCEL' : 'str', 'INSEE_COM' : 'str', 'CODE_CULTU' : 'str', 'Bdv' : 'str', 'rpg_lfgc_s' : 'str' , 'SURF_PARC' : 'float', 'VALEUR_CAD' : 'float', 'SURF_NON_U' : 'float'}}
	schemaCadastre = {'geometry' : 'Polygon', 'properties': {'idpar' : 'str', 'idcom' : 'str', 'valeur_cad' : 'float'}}
	crs = inputShape.crs
	driver = inputShape.driver

	#m = re.search('(.*)(.shp)', './Parcelle_PAT/Parcelle_PAT.shp')


	# PARTIE 1 : Transforme la couche de valeur cadastrale contenant des erreurs de géométries et des superpositions en couche de meilleure qualité

	# Ouvre la couche de la fusion des 3 départements existants dans le PAT
	with fiona.open('./productivite_shape/valeur_cadastrale_propre.shp' ,'w',driver=driver, crs=crs, schema=schemaCadastre) as shape_cadPropre:
		with fiona.open('./Parcelle_PAT/Parcelle_PAT.shp', 'r') as input:
			with fiona.open('./productivite_shape/valeurs_cadastrales.shp', 'r') as inputCadastre : # Ouvre la couche des parcelles cadastrales contenant leurs valeurs
				# Création de l'index spatial sur les parcelles cadastrales
				index = rtree.index.Index()
				i = 0
				for feat1 in inputCadastre : # Pour chacune des géométries
					if feat1['geometry'] != None : # Si la géométrie est non nulle
						geom1 = shape(feat1['geometry'])
						index.insert(i, geom1.bounds)
					i += 1

				liste_parcelles_traitees = [] # Liste qui contiendra le FID des parcelles déjà traitées
				for i in range(len(input)): # Nouvelle boucle traversant toutes les parcelles du PAT
							geom2 = shape(input[i]["geometry"]) # Stocke la géométrie de la parcelle traversée


							for fid in list(index.intersection(geom2.bounds)) : # Pour chacune des parcelles cadastrales ayant une intersection avec la parcelle du PAT
								if inputCadastre[fid]["geometry"] != None and fid not in liste_parcelles_traitees : # Si la parcelle n'a pas déjà été traitée et que sa geométrie est non nulle
									feat1 = inputCadastre[fid]
									geom1 = shape(feat1["geometry"])

									liste_parcelle_intersects = [] # Liste qui contiendra toutes les parcelles intersectant la parcelle de fid 'fid2'
									for fid2 in list(index.intersection(geom1.bounds)) : # Pour les parcelles intersectant les limites de cette parcelle
										if inputCadastre[fid2]["geometry"] != None  and fid != fid2: # S'il possède une géométrie et qu'il n'est pas lui-même
											feat12 = inputCadastre[fid2]
											geom12 = shape(feat12["geometry"])
											if geom1.intersection(geom12).area > 0 : # Si la surface d'intersection est supérieure à 0, donc si il y a superposition
												if fid not in liste_parcelles_traitees : # Si la parcelle 'fid' n'a pas déjà été traitée
													liste_parcelles_traitees.append(fid) # L'ajouter à la liste des parcelles traitées
												liste_parcelle_intersects.append(fid2) # Ajouter 'fid' à la liste des parcelles intersectant 'fid'

									if len(liste_parcelle_intersects) > 0 : # Si 'fid' intersecte 1 ou plusieurs parcelles
										geom1Bis = feat1["geometry"]
										surface = geom1.area
										valeur_cadastraleBis = inputCadastre[fid]["properties"]["valeur_cad"]
										newGeom = geom1Bis
										for y in range(len(liste_parcelle_intersects)) : # Pour chacune des parcelles intersectant 'fid'
											if liste_parcelle_intersects[y] in liste_parcelles_traitees :  # Si la parcelle a déjà été traitée
												geomY = shape(inputCadastre[liste_parcelle_intersects[y]]["geometry"]) # Stocker sa géométrie
												surfaceIntersect = shape(newGeom).intersection(geomY).area # Stocker la surface d'intersection

												# La nouvelle géometrie correspondra à l'ancienne sans la partie intersectant l'autre parcelle déjà traitée
												newGeom = mapping(shape(geom1Bis).difference(geomY))

												if newGeom["type"] != 'GeometryCollection' : # Si la nouvelle géométrie est vide (superposition complète de la parcelle 'fid2' sur 'fid')
													geom1Bis = newGeom
													valeur_cadastraleBis = valeur_cadastraleBis * (surface - surfaceIntersect) / surface

										# Recuperer le fid des parcelles dans liste_parcelles_intersects pour recuperer la valeur des properties
										recW = {}
										recW["geometry"] = geom1Bis

										recW["properties"] = {}
										recW["properties"]["idpar"] = inputCadastre[fid]["properties"]["idpar"]
										recW["properties"]["idcom"] = inputCadastre[fid]["properties"]["idcom"]
										recW["properties"]["valeur_cad"] = valeur_cadastraleBis

										shape_cadPropre.write(recW) # Ajoute cette entitée dans la nouvelle couche

				# Ajouter toutes les parcelles non traitées à la couche. Ces parcelles ne possèdent aucun problème d'intersection pouvant poser des problèmes potentiels
				cpt = 0
				for i in range(len(inputCadastre)) :
					if i not in liste_parcelles_traitees :
						#print cpt
						cpt += 1
						recW = {}
						recW["geometry"] = inputCadastre[i]["geometry"]

						recW["properties"] = {}
						recW["properties"]["idpar"] = inputCadastre[i]["properties"]["idpar"]
						recW["properties"]["idcom"] = inputCadastre[i]["properties"]["idcom"]
						recW["properties"]["valeur_cad"] = inputCadastre[i]["properties"]["valeur_cad"]

						shape_cadPropre.write(recW)

	# PARTIE 2 : Utilise la couche de valeur cadastrale de bonne qualité afin de l'associer avec la couche des parcelles du PAT
	# Cela résulte en une couche des parcelles du PAT avec un champ contenant leurs valeurs cadastrales tenant compte des superpositions des parcelles cadastrales avec celles du PAT

	# Création d'index spatial sur les parcelles du PAT
	index = rtree.index.Index()
	liste_valeur_cadastrale = {}
	liste_surface_non_utilise_PAT = {}
	i = 0
	for feat1 in inputShape :
		geom1 = shape(feat1['geometry'])
		index.insert(i, geom1.bounds)
		# Champ de la valeur cadastrale des parcelles PAT
		liste_valeur_cadastrale[i] = 0
		# Champ de la surface de la parcelle ne possédant pas de valeur cadastrale connu avec nos données MAJIC
		liste_surface_non_utilise_PAT[i] = geom1.area

		i += 1

	with fiona.open('./productivite_shape/Parcelle_PAT_valCad_test.shp' ,'w',driver=driver, crs=crs, schema=schema) as shape_cad:
		with fiona.open('./productivite_shape/valeur_cadastrale_propre.shp', 'r') as inputCadastre : # shape des parcelles cadastrales et de leurs valeurs

			i = 0
			for i in range(len(inputCadastre)): #Pour chacune des parcelles
				print(i)
				if inputCadastre[i]["geometry"] != None : # Si il y a une géométrie
					geom2 = shape(inputCadastre[i]["geometry"]).buffer(0) # Faire un buffer 0 pour corriger des erreurs de géométries potentielles

					for fid in list(index.intersection(geom2.bounds)): # Pour chaque parcelle de l'index i qui intersecte les limites d'une parcelle cadastrale
						feat1 = inputShape[fid]
						geom1 = shape(feat1["geometry"])

						if (geom2).intersects(geom1) : # Si les 2 géométries s'intersectent

							surface_intersection = geom2.intersection(geom1).area # Surface d'intersection

							# Incrémentation de la valeur cadastrale de la parcelle avec fonction du % de superposition entre la parcelle du PAT et celle du cadastre
							liste_valeur_cadastrale[fid] += inputCadastre[i]["properties"]["valeur_cad"] * surface_intersection / geom2.area

							# Décrémentation permettant de connaître la surface d'une parcelle PAT ne contenant pas de valeur cadastrale
							liste_surface_non_utilise_PAT[fid] -= float(surface_intersection)
				i += 1

		# Ajout des parcelles PAT avec les nouveaux champs contenant la valeur cadastrale et la surface de la parcelle n'ayant pas eu d'intersection avec une parcelle cadastrale
		for i in range(len(liste_valeur_cadastrale)) :
			recW = {}
			recW["geometry"]=inputShape[i]["geometry"]
			recW["properties"] = {}
			recW["properties"]["ID_PARCEL"] = inputShape[i]["properties"]["ID_PARCEL"]
			recW["properties"]["INSEE_COM"] = inputShape[i]["properties"]["INSEE_COM"]
			recW["properties"]["CODE_CULTU"] = inputShape[i]["properties"]["CODE_CULTU"]
			recW["properties"]["Bdv"] = inputShape[i]["properties"]["Bdv"]
			recW["properties"]["rpg_lfgc_s"] = inputShape[i]["properties"]["rpg_lfgc_s"]
			recW["properties"]["SURF_PARC"] = inputShape[i]["properties"]["SURF_PARC"]
			recW["properties"]["VALEUR_CAD"] = liste_valeur_cadastrale[i]
			recW["properties"]["SURF_NON_U"] = liste_surface_non_utilise_PAT[i]
			shape_cad.write(recW)
